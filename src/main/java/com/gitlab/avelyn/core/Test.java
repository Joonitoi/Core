package com.gitlab.avelyn.core;

import com.gitlab.avelyn.architecture.utilites.Components;
import com.gitlab.avelyn.core.base.Events;
import com.gitlab.avelyn.core.components.ComponentPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerJoinEvent;

public class Test extends ComponentPlugin {{
	onEnable(() -> {
		System.out.println("Enabled!");
	});
	addChild(Components.component(() -> {
		System.out.println("Enabled!!");
	})).addChild(Events.listen((PlayerJoinEvent event) -> {
		Bukkit.broadcastMessage("Welcome to the server");
	}));
	
}}