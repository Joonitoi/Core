package com.gitlab.avelyn.core.components;

import com.gitlab.avelyn.architecture.base.Parent;
import com.gitlab.avelyn.architecture.base.Toggleable;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.bukkit.Bukkit.getPluginManager;

public class ComponentPlugin extends JavaPlugin implements Toggleable, Parent<Toggleable> {
	private final List<Toggleable> children = new ArrayList<>();
	private final List<Runnable> enableListenable = new ArrayList<>();
	private final List<Runnable> disableListenable = new ArrayList<>();
	
	@NotNull
	public ComponentPlugin onEnable(@NotNull Runnable... listeners) {
		getEnableListenable().addAll(asList(listeners));
		return this;
	}
	
	@NotNull
	public ComponentPlugin onDisable(@NotNull Runnable... listeners) {
		getDisableListenable().addAll(asList(listeners));
		return this;
	}
	
	@NotNull
	public ComponentPlugin unregisterEnable(@NotNull Runnable... listeners) {
		getEnableListenable().removeAll(asList(listeners));
		return this;
	}
	
	@NotNull
	public ComponentPlugin unregisterDisable(@NotNull Runnable... listeners) {
		getDisableListenable().removeAll(asList(listeners));
		return this;
	}
	
	@NotNull
	public List<Runnable> getEnableListenable() {
		return enableListenable;
	}
	
	@NotNull
	public List<Runnable> getDisableListenable() {
		return disableListenable;
	}
	
	@Override
	public void onEnable() {
		children.forEach(Toggleable::enable);
		enableListenable.forEach(Runnable::run);
	}
	
	@Override
	public void onDisable() {
		disableListenable.forEach(Runnable::run);
		children.forEach(Toggleable::disable);
	}
	
	@NotNull
	@Override
	public ComponentPlugin enable() {
		if (!isEnabled())
			getPluginManager().enablePlugin(this);
		return this;
	}
	
	@NotNull
	@Override
	public ComponentPlugin disable() {
		if (isEnabled())
			getPluginManager().disablePlugin(this);
		return this;
	}
	
	@NotNull
	@Override
	public List<Toggleable> getChildren() {
		return children;
	}
}