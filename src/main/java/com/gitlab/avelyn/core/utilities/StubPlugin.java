package com.gitlab.avelyn.core.utilities;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginLoader;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.logging.Logger;

public class StubPlugin implements Plugin {
	private final String name;
	private final BooleanSupplier enabled;
	private boolean naggable;
	
	public StubPlugin(String name, BooleanSupplier enabled) {
		this.name = name;
		this.enabled = enabled;
	}
	
	public boolean isNaggable() {
		return naggable;
	}
	
	@Override
	public void setNaggable(boolean naggable) {
		this.naggable = naggable;
	}
	
	@Override
	public boolean isEnabled() { return enabled.getAsBoolean(); }
	
	@Override
	public String getName() { return name; }
	
	@Override
	public Server getServer() { return Bukkit.getServer(); }
	
	@Override
	public Logger getLogger() { return Bukkit.getLogger(); }
	
	public List<String> onTabComplete(CommandSender a, Command b, String c, String[] d) { return stub(); }
	
	public boolean onCommand(CommandSender a, Command b, String c, String[] d) {
		return stub();
	}
	
	public File getDataFolder() { return stub(); }
	
	public PluginDescriptionFile getDescription() { return stub(); }
	
	public FileConfiguration getConfig() { return stub(); }
	
	public InputStream getResource(String s) { return stub(); }
	
	public void saveConfig() { stub(); }
	
	public void saveDefaultConfig() { stub(); }
	
	public void saveResource(String a, boolean b) { stub(); }
	
	public void reloadConfig() { stub(); }
	
	public PluginLoader getPluginLoader() { return stub(); }
	
	public void onDisable() { stub(); }
	
	public void onLoad() { stub(); }
	
	public void onEnable() { stub(); }
	
	public ChunkGenerator getDefaultWorldGenerator(String a, String b) { return stub(); }
	
	private static <T> T stub() {
		throw new UnsupportedOperationException();
	}
}